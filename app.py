from flask import Flask, render_template, request, url_for, redirect, request
from funcs import *

create_tables_if_not_exists()

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')


@app.route("/process_index_form", methods=["POST"])
def process_index_form():
    f = request.files['file']
    handle_file_submission(f)
    return redirect(url_for("index"))


@app.route("/view_submissions")
def view_submissions():
    query = '''
    SELECT * FROM pdfs;
    '''
    df = pd.read_sql_query(con=conn, sql=query)
    #return render_template('index.html')
    return df.to_html()


@app.route("/submit_tag")
def submit_tag():
    return render_template("submit_tag.html")


@app.route("/handle_submitted_tag", methods=["POST"])
def handle_submitted_tag():
    insert_tag(request.form['tag_name'])
    return redirect(url_for("view_tags"))

@app.route("/view_tags")
def view_tags():
    query = '''
    SELECT * FROM tags;
    '''
    df = pd.read_sql_query(con=conn, sql=query)
    #return render_template('index.html')
    return df.to_html()


@app.route("/login")
def login():
    return "Hello World!"


@app.route("/logout")
def logout():
    return "Hello World!"

app.run(debug=True, host='0.0.0.0', port=80)
