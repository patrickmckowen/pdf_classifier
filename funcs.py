'''



'''
import subprocess
import collections

import pandas as pd

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
from werkzeug import secure_filename
import sqlite3
import hashlib
import os
# Python program to find SHA256 hash string of a file
import hashlib

conn = sqlite3.connect("main.db", check_same_thread=False)
c = conn.cursor()

'''
# Python program to find SHA256 hexadecimal hash string of a file
import hashlib

filename = input("Enter the input file name: ")
with open(filename,"rb") as f:
    bytes = f.read() # read entire file as bytes
    readable_hash = hashlib.sha256(bytes).hexdigest();
    print(readable_hash)
'''
'''
def get_sha256(filename):
    sha256_hash = hashlib.sha256()
    with open(filename, "rb") as f:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096),b""):
            sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()
'''

def get_sha256(fileobj):
    sha256_hash = hashlib.sha256()
    # Read and update hash string value in blocks of 4K
    for byte_block in iter(lambda: fileobj.read(4096),b""):
            sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()

def create_tables_if_not_exists():
    c.execute('''
    create table if not exists pdfs (
    pdfID INTEGER PRIMARY KEY AUTOINCREMENT,
    submitted_file_name TEXT, 
    file_text TEXT UNIQUE,
    sha256 TEXT UNIQUE,
    upload_date TEXT);
    ''')
   
    c.execute('''
    create table if not exists tags (
    pdfID INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT);
    ''')
    
    c.execute('''
    create table if not exists tags_pdfs (
      tag_pdfID INTEGER PRIMARY KEY AUTOINCREMENT,
      pdf INTEGER,
      tag INTEGER,
      FOREIGN KEY (pdf) REFERENCES pdfs(pdfID),
      FOREIGN KEY (tag) REFERENCES tags(tagID));
    ''')
    conn.commit() 

def text_from_file(*file_paths):
    for file_path in file_paths:
        args = ["pdftotext",
                '-enc',
                'UTF-8',
                file_path,
                '-']
        res = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = res.stdout.decode('utf-8')
        return output

def handle_file_submission(submitted_file):
    #submitted_file = request.files['file']
    file_path = os.path.join("uploads", secure_filename(submitted_file.filename))
    text = text_from_file(file_path)
    c.execute('''
    INSERT OR IGNORE INTO pdfs (submitted_file_name, sha256, file_text, upload_date)
    VALUES (?, ?, ?, datetime('now'))''', (submitted_file.filename, get_sha256(file_path), text))
    conn.commit()
    submitted_file.save(file_path)



def insert_tag(tag_name):
    c.execute('''
    INSERT INTO tags (name)
    VALUES (?)''', (tag_name,))
    conn.commit()


